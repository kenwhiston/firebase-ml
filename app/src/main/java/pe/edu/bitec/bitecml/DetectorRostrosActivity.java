package pe.edu.bitec.bitecml;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark;
import com.pepperonas.materialdialog.MaterialDialog;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DetectorRostrosActivity extends AppCompatActivity
        implements View.OnClickListener {

    private ImageView img;
    private TextView txtResultado;
    private Button btnAnalizar;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detector_rostros);

        img = findViewById(R.id.img);
        txtResultado =  findViewById(R.id.txtResultado);
        btnAnalizar =  findViewById(R.id.btnAnalizar);

        context = this;

        //cacheo
        Picasso.get().load(R.drawable.img1).into(img);

        btnAnalizar.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnAnalizar){

            Bitmap bitmap = null;

            Drawable drawable = context.getResources().getDrawable(R.drawable.img1);
            // convert drawable to bitmap
            bitmap = ((BitmapDrawable)drawable).getBitmap();

            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);


            ///seccion de Firebase
            FirebaseVisionFaceDetectorOptions options =
                    new FirebaseVisionFaceDetectorOptions.Builder()
                            .setModeType(FirebaseVisionFaceDetectorOptions.ACCURATE_MODE)
                            .setLandmarkType(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
                            .setClassificationType(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
                            .setMinFaceSize(0.085f)
                            .setTrackingEnabled(true)
                            .build();

            FirebaseVisionFaceDetector detector = FirebaseVision.getInstance()
                    .getVisionFaceDetector(options);

            final MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(this)
                    .title("Firebase")
                    .message("Estamos procesando...")
                    .cancelable(false).canceledOnTouchOutside(false);
            materialDialog.show();

            Task<List<FirebaseVisionFace>> result =
                    detector.detectInImage(image)
                            .addOnSuccessListener(
                                    new OnSuccessListener<List<FirebaseVisionFace>>() {
                                        @Override
                                        public void onSuccess(List<FirebaseVisionFace> faces) {
                                            // Task completed successfully
                                            // ...

                                            materialDialog.dismissListener(new MaterialDialog.DismissListener() {
                                                @Override
                                                public void onDismiss() {
                                                    super.onDismiss();
                                                    Toast.makeText(context, "onDismiss", Toast.LENGTH_SHORT).show();
                                                }
                                            });

                                            String cadena = "ROSTROS ENCONTRADOS: "+String.valueOf(faces.size())+" || ";

                                            for (FirebaseVisionFace face : faces) {
                                                Rect bounds = face.getBoundingBox();
                                                float rotY = face.getHeadEulerAngleY();  // Head is rotated to the right rotY degrees
                                                float rotZ = face.getHeadEulerAngleZ();  // Head is tilted sideways rotZ degrees

                                                cadena = cadena + "PERSONA [ ";

                                                cadena = cadena + " || rotY: "+String.valueOf(rotY);
                                                cadena = cadena + " || rotZ: "+String.valueOf(rotZ);

                                                // If landmark detection was enabled (mouth, ears, eyes, cheeks, and
                                                // nose available):
                                                FirebaseVisionFaceLandmark leftEar = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EAR);
                                                if (leftEar != null) {
                                                    FirebaseVisionPoint leftEarPos = leftEar.getPosition();
                                                    cadena = cadena + " || leftEarPosX: "+String.valueOf(leftEarPos.getX());
                                                    cadena = cadena + " || leftEarPosY: "+String.valueOf(leftEarPos.getY());
                                                    cadena = cadena + " || leftEarPosZ: "+String.valueOf(leftEarPos.getZ());
                                                }

                                                // If classification was enabled:
                                                if (face.getSmilingProbability() != FirebaseVisionFace.UNCOMPUTED_PROBABILITY) {
                                                    float smileProb = face.getSmilingProbability();
                                                    cadena = cadena + " || getSmilingProbability: "+String.valueOf(smileProb);
                                                }
                                                if (face.getRightEyeOpenProbability() != FirebaseVisionFace.UNCOMPUTED_PROBABILITY) {
                                                    float rightEyeOpenProb = face.getRightEyeOpenProbability();
                                                    cadena = cadena + " || rightEyeOpenProb: "+String.valueOf(rightEyeOpenProb);

                                                }

                                                // If face tracking was enabled:
                                                if (face.getTrackingId() != FirebaseVisionFace.INVALID_ID) {
                                                    int id = face.getTrackingId();
                                                }

                                                cadena = cadena + " ] ";

                                            }

                                            txtResultado.setText(cadena);

                                        }
                                    })
                            .addOnFailureListener(
                                    new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            // Task failed with an exception
                                            // ...
                                            Toast.makeText(context,"Error: "+e.getMessage(),Toast.LENGTH_LONG).show();
                                        }
                                    });

        }
    }
}
