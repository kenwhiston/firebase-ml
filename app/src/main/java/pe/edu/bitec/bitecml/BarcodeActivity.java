package pe.edu.bitec.bitecml;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BarcodeActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img;
    private TextView txtResultado;
    private Button btnAnalizar;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);

        img = findViewById(R.id.img);
        txtResultado =  findViewById(R.id.txtResultado);
        btnAnalizar =  findViewById(R.id.btnAnalizar);

        context = this;

        //cacheo
        Picasso.get().load(R.drawable.code).into(img);

        btnAnalizar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnAnalizar){

            Bitmap bitmap = null;

            //bitmap = BitmapFactory.decodeResource(context.getResources(),
            //R.drawable.imagen);
            //bitmap = drawableToBitmap(getResources().getDrawable(R.drawable.imagen));

            Drawable drawable = context.getResources().getDrawable(R.drawable.code);
            // convert drawable to bitmap
            bitmap = ((BitmapDrawable)drawable).getBitmap();

            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);

            FirebaseVisionBarcodeDetectorOptions options =
                    new FirebaseVisionBarcodeDetectorOptions.Builder()
                            .build();

            //.setBarcodeFormats(
            //                                    FirebaseVisionBarcode.FORMAT_QR_CODE,
            //                                    FirebaseVisionBarcode.FORMAT_AZTEC)

            FirebaseVisionBarcodeDetector detector = FirebaseVision.getInstance()
                    .getVisionBarcodeDetector();

            Task<List<FirebaseVisionBarcode>> result = detector.detectInImage(image)
                    .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                        @Override
                        public void onSuccess(List<FirebaseVisionBarcode> barcodes) {
                            // Task completed successfully
                            // ...
                            String cadena="";
                            cadena = "BARCODES ENCONTRADOS: "+String.valueOf(barcodes.size())+" || ";

                            for (FirebaseVisionBarcode barcode: barcodes) {
                                Rect bounds = barcode.getBoundingBox();
                                Point[] corners = barcode.getCornerPoints();

                                String rawValue = barcode.getRawValue();

                                int valueType = barcode.getValueType();
                                // See API reference for complete list of supported types

                                cadena = cadena + "BARCODE [ ";

                                cadena = cadena + " || rawValue: "+String.valueOf(rawValue);
                                cadena = cadena + " || valueType: "+String.valueOf(valueType);

                                cadena = cadena + " ] ";

                            }

                            txtResultado.setText(cadena);

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Task failed with an exception
                            // ...
                            Toast.makeText(context,"Error: "+e.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    });

        }
    }
}
