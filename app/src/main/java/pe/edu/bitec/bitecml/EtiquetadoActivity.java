package pe.edu.bitec.bitecml;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.label.FirebaseVisionLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionLabelDetector;
import com.google.firebase.ml.vision.label.FirebaseVisionLabelDetectorOptions;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EtiquetadoActivity extends AppCompatActivity
implements View.OnClickListener{

    private ImageView img;
    private TextView txtResultado;
    private Button btnAnalizar;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etiquetado);

        img = findViewById(R.id.img);
        txtResultado =  findViewById(R.id.txtResultado);
        btnAnalizar =  findViewById(R.id.btnAnalizar);

        context = this;

        //cacheo
        Picasso.get().load(R.drawable.etiqueta1).into(img);

        btnAnalizar.setOnClickListener(this);

        //obtener token FCM
        String token = FirebaseInstanceId.getInstance().getToken();

        Log.d("FCM",token);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnAnalizar){

            FirebaseVisionLabelDetectorOptions options =
                    new FirebaseVisionLabelDetectorOptions.Builder()
                            .setConfidenceThreshold(0.8f)
                            .build();

            Bitmap bitmap = null;

            //bitmap = BitmapFactory.decodeResource(context.getResources(),
            //R.drawable.imagen);
            //bitmap = drawableToBitmap(getResources().getDrawable(R.drawable.imagen));

            Drawable drawable = context.getResources().getDrawable(R.drawable.etiqueta1);
            // convert drawable to bitmap
            bitmap = ((BitmapDrawable)drawable).getBitmap();

            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);

            FirebaseVisionLabelDetector detector = FirebaseVision.getInstance()
                    .getVisionLabelDetector();
            // Or, to set the minimum confidence required:
            //FirebaseVisionLabelDetector detector = FirebaseVision.getInstance()
                    //.getVisionLabelDetector(options);

            Task<List<FirebaseVisionLabel>> result =
                    detector.detectInImage(image)
                    .addOnSuccessListener(
                            new OnSuccessListener<List<FirebaseVisionLabel>>() {
                                @Override
                                public void onSuccess(List<FirebaseVisionLabel> labels) {
                                    // Task completed successfully
                                    // ...

                                    String cadena="";
                                    cadena = "LABELS ENCONTRADOS: "+String.valueOf(labels.size())+" || ";

                                    for (FirebaseVisionLabel label: labels) {
                                        String text = label.getLabel();
                                        String entityId = label.getEntityId();
                                        float confidence = label.getConfidence();

                                        cadena = cadena + "LABEL [ ";

                                        cadena = cadena + " || text: "+String.valueOf(text);
                                        cadena = cadena + " || entityId: "+String.valueOf(entityId);
                                        cadena = cadena + " || confidence: "+String.valueOf(confidence);

                                        cadena = cadena + " ] ";

                                    }

                                    txtResultado.setText(cadena);

                                }
                            })
                    .addOnFailureListener(
                            new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    // Task failed with an exception
                                    // ...
                                }
                            });


        }
    }
}
